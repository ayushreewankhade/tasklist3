package com.SpringRest.springrest.services;

import com.SpringRest.springrest.entities.Course;

public interface CourseService {

	public Iterable<Course> getAllCourses();

	public Course getCourseById(Long courseId);

	public Course addCourse(Course course);

	public void deleteCourse(Long courseId);

	Course updateCourse(Course course, Long courseId);
	
	

	
}
