package com.SpringRest.springrest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SpringRest.springrest.dao.CourseDao;
import com.SpringRest.springrest.entities.Course;
@Service
public class CourseServiceImpl implements CourseService{

	@Autowired
	private CourseDao courseDao;
	
	//public CourseServiceImpl() {}
	
	@Override
	public Iterable<Course> getAllCourses() {
		return courseDao.findAll();
	}

	
	@Override
	public Course getCourseById(Long courseId) {
		return courseDao.getReferenceById(courseId);
		
	}
	
	@Override
	public Course addCourse(Course course) {
		courseDao.save(course);
		return course;
	}
	
	
	@Override
	public void deleteCourse(Long courseId) {
	    courseDao.deleteById(courseId);
	}

    @Override
	public Course updateCourse(Course course, Long courseId) {
		courseDao.save(course);
	    return course;
	}

}

