package com.SpringRest.springrest.controller;

//import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.SpringRest.springrest.entities.Course;
import com.SpringRest.springrest.services.CourseService;

@RestController 
public class CourseController {
	@Autowired
	private CourseService courseService;
	
	@GetMapping("/courses")
	public Iterable<Course> getAllCourses(){
		return this.courseService.getAllCourses();

	}
	  
	@GetMapping("/courses/{courseId}") 
	public ResponseEntity<Course> getCourseById(@PathVariable("courseId") Long courseId)throws Exception {
		Course c= courseService.getCourseById(courseId);
		return ResponseEntity.ok().body(c); 
	}
	
	@PostMapping("/courses")
	public ResponseEntity<Course> addCourse( @RequestBody Course course, HttpServletRequest request) throws Exception{
	Course createdCourse=courseService.addCourse(course);
		return ResponseEntity.ok().body(createdCourse);
	}
	
	@PutMapping("/courses/{courseId}")
	public ResponseEntity<Course> updateCourse(@RequestBody Course course,@PathVariable("courseId") Long courseId){
		Course updatedCourse=courseService.addCourse(course);
		try {
			this.courseService.updateCourse(course,courseId); 
			return ResponseEntity.ok().body(updatedCourse);
		} 
		 catch(Exception e) {
			e.printStackTrace();
			 return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		 }  
		}  
	
	@DeleteMapping("/courses/{courseId}")
	public ResponseEntity<HttpStatus> deleteCourse(@PathVariable("courseId") Long courseId){
	try{
		this.courseService.deleteCourse(courseId);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	catch (Exception e) {
	    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	
	}}
} 